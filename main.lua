require 'helpers/colors'
local Timer = require 'lib/knife.timer'

love.graphics.setDefaultFilter("nearest")
score = 0
width = 320
height = 240
gridSize = 7
spriteSize = 24

board = {}

flavors = {}
for i = 1, 5 do
    flavors[i] = love.graphics.newImage("assets/yog_"..i..".png")
end

function clearBoard()
    for r = 1, gridSize do
        board[r] = {}
        for c = 1, gridSize do
            board[r][c] = 0
        end
    end
end

clearBoard();

function love.load()
    colors = {}
    state = {}
    colors.gridBorder = "FFFFFFFF"
    state.currentFlavor = love.math.random(#flavors)
    state.currentColumn = 4
    bitfont = love.graphics.newFont("assets/04B_03__.TTF", 16)
    love.graphics.setFont(bitfont)
    Timer.every(.1, function () doGravity() end)
end

function doGravity()
    for r = gridSize - 1, 1, -1 do
        for c = 1, gridSize do
            if board[r + 1][c] == 0 then
                board[r + 1][c] = board[r][c]
                board[r][c] = 0
            end
        end
    end
    checkColissions()
end

function checkColissions()
    
    for r = 1, gridSize do
        local searchLength = 1
        local searchColor = 0
        for c = 1, gridSize do
            if board[r][c] == searchColor then
                searchLength = searchLength + 1
            end
            if board[r][c] ~= searchColor or c == gridSize then
                if c == gridSize then
                    c = c + 1
                end
                if searchColor ~= 0 and searchLength >= 4 then
                    for i = c - searchLength, c - 1 do
                        board[r][i] = 0
                    end
                    score = score + (searchLength ^ 2)
                end
                searchColor = board[r][c]
                searchLength = 1
            end
        end
    end
    
end

function printBoard()
    -- io.write("\027[H\027[2J")
    for k, v in pairs(board) do
        io.write(k..">")
        for ik, iv in pairs(board[k]) do
            io.write(iv)
        end
        io.write("\n")
    end
end

function love.keypressed(key)
    if key == "left" then
        if state.currentColumn > 1 then
            state.currentColumn = state.currentColumn - 1
        end
    end
    if key == "right" then
        if state.currentColumn < gridSize then
            state.currentColumn = state.currentColumn + 1
        end
    end
    if key == "space" then
        local finalRow = 0
        board[1][state.currentColumn] = state.currentFlavor
        checkColissions()
        state.currentFlavor = love.math.random(#flavors)
    end
end

function love.update(dt)
    Timer.update(dt)
end

function love.draw()
    local lg = love.graphics
    lg.scale(2, 2)
    lg.setDefaultFilter("nearest")
    setColor(colors.gridBorder)
    lg.print("SCORE: "..score)
    for r = 1, gridSize do
        for c = 1, gridSize do
            setColor(colors.gridBorder)
            lg.rectangle("line", c * spriteSize, r * spriteSize, spriteSize, spriteSize)
            if (board[r][c] ~= 0) then
                lg.draw(flavors[board[r][c]], c * spriteSize, r * spriteSize)
            end
        end
    end
    lg.draw(flavors[state.currentFlavor], state.currentColumn * spriteSize, 0)
end
