function love.conf(t)
  t.identity = "bear.gamejamarg.game"
  t.version = "11.2"
  t.window.title = "" -- "🧸.tracker"
  t.window.width = 640
  t.window.height = 480
end